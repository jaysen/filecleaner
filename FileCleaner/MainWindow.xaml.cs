﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileCleaner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            searchBtn.Click += SearchAndDisplay;
            searchAndDeleteBtn.Click += SearchAndDelete;
        }

        public void SearchAndDisplay(object sender, RoutedEventArgs e)
        {
            resultsBox.Text += String.Format("Searching in {0} for string='{1}' - to display only! \n", folderBox.Text, searchStrBox.Text);
            var fileList = DirTools.GetMatchingFileList(folderBox.Text, searchStrBox.Text);

            if (fileList != null) {
                if (fileList.Count > 100) {
                    resultsBox.Text += String.Format("Found more than a 1000 files!!! - not displaying them here. \n");
                }
                else {
                    foreach (var file in fileList) {
                        resultsBox.Text += String.Format(" - {0} \n", file);
                    }
                }
                resultsBox.Text += String.Format("Found {0} files", fileList.Count);
                resultsBox.Text += "\n\n";
                Console.WriteLine("found {0} files", fileList.Count);

            }
        }

        public void SearchAndDelete(object sender, RoutedEventArgs e)
        {
            resultsBox.Text += String.Format("Searching in {0} for string='{1}' - to Delete!! \n", folderBox.Text, searchStrBox.Text);
            DirTools.DeleteMatchingFiles(folderBox.Text, searchStrBox.Text);

            resultsBox.Text += String.Format("Found and deleted {0} files", DirTools.GetFileCount());
            resultsBox.Text += "\n\n";
            Console.WriteLine("found and deleted {0} files", DirTools.GetFileCount());
        }
    }
}

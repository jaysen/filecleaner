﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCleaner
{
    class DirTools
    {
        static string Folder { get; set; }
        static List<string> FileList { get; set; }


        public static int GetFileCount()
        {
            return FileList.Count;
        }

        /// <summary>
        /// Gets the FileList that matches search string in passed in folder and subfolders
        /// </summary>
        /// <param name="sDir">folder to start search</param>
        /// <param name="srchStr">search string</param>
        /// <returns>list of matching files </returns>
        public static List<string> GetMatchingFileList(string sDir, string srchStr)
        {
            emptyFileList();
            SetMatchingFilesInDirs(sDir, srchStr);
            return FileList;
        }

        /// <summary>
        /// Deletes all files in folder and subfolders that match search string
        /// </summary>
        /// <param name="sDir">folder to search</param>
        /// <param name="srchStr">search string</param>
        public static void DeleteMatchingFiles(string sDir, string srchStr)
        {
            emptyFileList();
            SetMatchingFilesInDirs(sDir, srchStr);
            DeleteFiles();
        }


        #region Private methods

        // Sets FileList based on root Dir and searchstring
        static void SetMatchingFilesInDirs(string sDir, string srchStr)
        {
            matchFiles(sDir, srchStr);
        }

        // recursive internal method to match files in all subfolders ...
        static void matchFiles(string sDir, string srchStr )
        {
            try {
                foreach (string f in Directory.GetFiles(sDir, srchStr)) {
                    FileList.Add(f);
                    //Console.WriteLine("found a file! " + f );
                }
                foreach (string d in Directory.GetDirectories(sDir)) {
                    matchFiles(d, srchStr);
                }
            }
            catch (System.Exception excpt) {
                Console.WriteLine(excpt.Message);
            }
        }

        static void emptyFileList()
        {
            if (FileList == null) {
                FileList = new List<string>();
            }
            else {
                FileList.Clear();
            }
        }

        // Deletes all files that have been loaded into FileList 
        static void DeleteFiles()
        {
            if (FileList != null) {
                foreach (var file in FileList) {
                    try {
                        File.Delete(file);
                    }
                    catch (Exception excpt) {

                        Console.WriteLine(excpt.Message); ;
                    }
                }
            }
        }

        #endregion
    }
}
